<!DOCTYPE html>
<html> 
	<head>
		<title> Form Sign Up </title>
	</head>
	
	<body>
		<h1> Buat Account Baru! </h1>
		<h3> Sign Up Form </h3>
		<form action="welcomee" method="POST">
            @csrf
		<label for="firstname_user"> First name: </label>
		<br><br>
		<input type="text" name="firstname_user" value="" id="firstname_user">
		<br><br>
		<label for="lastname_user"> Last name: </label>
		<br><br>
		<input type="text" name="lastname_user" value="" id="lastname_user">
		<br><br>
		<label> Gender: </label> 
		<br><br>
		<input type="radio" name="gender" value="Male">Male <br>
		<input type="radio" name="gender" value="Female">Female <br>
		<input type="radio" name="gender" value="Other">Other <br>
		<br>
		<label> Nationality: </label>
		<br><br>
		<select>
			<option value="Indonesia">Indonesian</option>
			<option value="Singapore">Singapore</option>
			<option value="Malaysia">Malaysia</option>
			<option value="Australian">Australian</option>
		</select>
		<br><br>
		<label> Language Spoken: </label>
		<br><br>
		<input type="checkbox" name="language_user" value="Bahasa Indonesia">Bahasa Indonesia <br>
		<input type="checkbox" name="language_user" value="English">English <br>
		<input type="checkbox" name="language_user" value="Other">Other <br>
		<br>
		<label for="bio_user"> Bio: </label> 
		<br><br>
		<textarea name="bio_user" cols="30" rows="10" id="bio_user"></textarea>
		<br>
		<input type="submit" value="Sign Up">
		</form>
	</body> 
</html>